# 定义一个类
class Player
    # initialize是构造函数
    def initialize(name="tom")
        # @相当于Java里的this
        @name = name
    end

    def show()
        puts "player: #{@name}"
    end

end

# 初始化类的时候，点new()就相当于调用构造器，但是不用声明
tom = Player.new()
tom.show()

jerry = Player.new("jerry")
jerry.show()
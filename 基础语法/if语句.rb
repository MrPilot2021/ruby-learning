# 如果没有else，直接if-end
if true 
    puts "真"
end

# 如果涉及到else，只在最后加end，then加不加都行
point = 40
if point>=50 
    puts "aaa"
elsif point>=30 && point <50 
    puts "bbb"
else
    puts "ccc"
end

# 三项运算
puts point >= 30 ? "你赢了" : "你输了"

# unless关键字：只有在表达式不成立时才执行（不常用）
point = 10
unless point<20 
    # 可以翻译为：除非point<20，否则就执行下面的语句
    puts "大于20"
else
    puts "小于20"
end

# case-when
day = 8
case day
    when 0,7
        puts "星期日"
    when 1
        puts "星期一"
    when 2
        puts "星期二"
    else
        raise Exception.new("没这天")
end


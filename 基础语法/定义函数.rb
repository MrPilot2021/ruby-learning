# 普通函数
def sayHello
    puts "helloworld"
end

# 执行的时候带不带参数都行
sayHello

# 带参数
def hello(name)
    puts "helloworld, #{name}"
end

hello("feh")

# 默认参数
def hello2(name="Alex")
    puts "helloworld, #{name}"
end

hello2
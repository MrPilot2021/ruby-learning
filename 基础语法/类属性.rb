
class Game
    # 用attr_accessor声明的属性可以从外部访问。等效于Java的public
    attr_accessor :price
    attr_accessor :title

    def initialize(title = "赛博朋克", price = 200)
        @title = title
        @price = price
    end

    def show()
        puts "标题：#{@title}"
        puts "价格：#{@price}"
    end
end

mygame = Game.new()
mygame.show()

# respond_to?也可以查看属性是否可用，to_s是转成string
puts "title is " + mygame.respond_to?("title").to_s
puts "price is " + mygame.respond_to?("price").to_s

mygame.price = 150
mygame.title = "赛博朋克DLC"
mygame.show()
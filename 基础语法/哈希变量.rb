# 哈希变量用花括号表示

# 可以当Java的hashmap用
age = {
    "tom"=> 18,
    "jerry" => 27,
    "alex" =>24
}
puts age
puts age["alex"]

# 也可以当Json用
player = {
    name: "tom",
    age: 20,
    weight: 76
}
puts player
# 引用对象属性的方式：对象名[:属性名]
puts player[:name]
puts player[:age]
# 数字输出别忘了转字符串
puts "体重：" + player[:weight].to_s
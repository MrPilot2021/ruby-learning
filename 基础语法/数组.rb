# 定义数组，并打印
games = ["绝地逃生","怪猎","赛博朋克"]
# 普通打印
puts games
# 指定格式连接输出
puts games.join(";")

# 判断是否为一个数组（直接判断对象有没有each方法）
if games.respond_to?("each")
    puts "确实是一个数组" 

# 遍历方法1：|代表自定义的“形参”|
games.each do |game|
    puts "我爱#{game}"
end

# 遍历方法2：带上索引
games.each_with_index do |game, i|
    puts "第#{i}个游戏：#{game}"
end


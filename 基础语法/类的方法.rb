
class Game

    # 构造器
    def initialize(title = "赛博朋克", price = 200)
        @title = title
        @price = price
    end

    # 没特殊标记的方法：基本都是实例方法
    def show1()
        puts "标题：#{@title}"
        puts "价格：" + @price.to_s
    end

    # 可以出现空方法
    def show2()
    end

    # self标记的都是静态方法
    def self.show3(name)
        puts "I love this game: #{name}"
    end

    def self.show4
        puts "没有输入的静态方法"
    end
end

# 静态方法：不用实例化就可以执行(和Java一样)
# 两种调用方式：点和双冒号
Game.show3("cyberpunk2077")
Game::show3("塞尔达")
# 对于无参的方法，调用时有无括号都可以
Game.show4
Game.show4()
Game::show4

# 和Java不同的地方：实例无法调用静态方法
genshen = Game.new("genshen impact", 100)
genshen.show3("genshen")
genshen.show4

# 一些自带方法：

# 显示类携带的实例方法，true显示包括自带方法，false只显示自定义的方法
puts Game.instance_methods(false)

cyberpunk = Game.new()
# 判断对象是否存在某个方法（问号必须加）
if  cyberpunk.respond_to?("show1")
    # 主动执行
    cyberpunk.send("show1")
end


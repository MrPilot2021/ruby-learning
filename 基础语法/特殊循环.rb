gamelist = ["赛博朋克","塞尔达","原神"]

# each循环
gamelist.each do |game|
    puts "游戏：#{game}"
end

gamelist.each { |game|
    puts "游戏：#{game}"
}

# each循环升级版
gamelist.each_with_index do |game,index|
    puts index.to_s + "." + game
end

# time循环：只指定次数
5.times do |i|
    puts "第#{i}次time循环"
end

# step循环：可以指定步长
# 从1开始，步长为3，循环到11（只到10，够不到11）
1.step(11,3) do |i|
    puts "#{i}"
end

# upto循环：和step很像，只不过步长为1
# 从2循环到5
2.upto(5) do |i|
    puts "upto=" + i.to_s
end

# downto循环：同上
5.downto(2) do |i|
    puts "downto=" + i.to_s
end

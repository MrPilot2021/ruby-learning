# 循环1-5（两个点）
for num in 1..5 do
    puts num
end

# 循环1-4（三个点）
for num in 1...5 do
    puts num
end

# for循环遍历数组
gamelist = ["赛博朋克","塞尔达","原神"]
for game in gamelist do
    puts game
end

# while循环
index = 0
while index < 5 do
    puts "index=" + index.to_s
    index+=1
end

# until遍历
index = 0
# 在index=5之前都会执行
until index == 5 do
    puts "index=" + index.to_s
    index+=1
end


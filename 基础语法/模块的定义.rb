# 模块类似class，但是不同
# 只有静态方法可以被其他类从外部访问
module MyModule

    # 外部可以存取（必须要大写）
    Version = "0.0.1"

    # 不加self就是内部方法，不能直接由module访问（但可以由class引用后实例化的对象访问）
    def innerMethod
        return Version
    end

    def innerMethod2(a,b)
        return a+b
    end

    # 外部可以访问静态方法
    def self.staticMethod
        return Version
    end

    # 用module_function将内部方法定义为静态方法：也可以从外部访问
    module_function :innerMethod2

end

# module无需实例化即可被调用
puts MyModule::Version
puts MyModule::staticMethod
# puts MyModule::innerMethod
puts MyModule::innerMethod2(1,3)

# 在类中使用 
class BaseClass include MyModule
end

# 可以通过class引用module中的变量
puts BaseClass::Version

# 不可以通过class引用module中的内部方法
# puts BaseClass::innerMethod

# 不可以通过class引用module中的静态方法
# puts BaseClass::staticMethod

# 可以通过class实例化的对象，访问module的内部方法
myclass = BaseClass.new
puts myclass.innerMethod





class Game
    # 用attr_accessor声明的属性可以从外部访问。等效于Java的public
    attr_accessor :price
    attr_accessor :title

    def initialize(title = "赛博朋克", price = 200)
        @title = title
        @price = price
    end

    def show()
        puts "标题：#{@title}"
        puts "价格：#{@price}"
    end

    def self.toStr
        puts "子类静态方法"
    end
end

# 小于号就等于Java的extend
class SteamGame < Game
    def steamInfo
        puts "G胖万岁"
    end
end

# 子类可以调用父类的静态方法
SteamGame.toStr

# 子类调用父类的实例方法
mygame = SteamGame.new("原神", price = 89)
mygame.show

# 子类调用自己的方法
mygame.steamInfo
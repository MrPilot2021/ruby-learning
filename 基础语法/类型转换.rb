# 字符转整数
a = "50"
b = "40"
c = "89"

puts a+b+c
puts a.to_i + b.to_i + c.to_i

# 整数转字符串
d = 90
puts  d.to_s + 178.to_s # 数值上可以直接操作

# 整数转浮点
puts d.to_f
puts 23.to_f

# 浮点转整数(直接截取整数部分，而不是四舍五入)
puts 12.99.to_i
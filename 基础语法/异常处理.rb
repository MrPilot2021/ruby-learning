begin # try
    puts "有可能发生的异常"
    10 / 0
rescue => e # catch
    puts "发生错误:" + e.to_s
else 
    puts "无错误"
ensure # finally
    puts "无论是否发生错误，都执行"
end